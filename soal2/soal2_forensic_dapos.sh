#!/bin/bash

path=forensic_log_website_daffainfo_log
log=~/log_website_daffainfo.log
avg=ratarata.txt
res=result.txt

## A
## Membuat folder bernama forensic_log_website_daffainfo_log.
[ ! -d $path ] && mkdir $path
[ ! -f $path/$avg ] && touch $path/$avg
[ ! -f $path/$res ] && touch $path/$res

## B
## Menampilkan rata rata request perjam dan memasukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt
awk -F: 'NR>1{print $3}'  $log| sort | uniq -c | 
awk '{sum+=$1} END {printf "Rata-rata serangan adalah sebanyak %d requests per jam\n", sum/(NR-1)}' >> $path/$avg

## C 
## Menampilkan IP yang paling banyak melakukan request dan memasukkan outputnya ke dalam file result.txt
awk -F: 'NR>1{print $1}' $log | sort | uniq -c | sort -rn | 
awk 'NR==1{gsub(/"/,"")}1' | awk 'NR==1{printf "IP yang paling banyak mengakses server adalah:  %s sebanyak %s requests\n\n", $2, $1}' >> $path/$res

## D
## Menampilkan banyak request yang menggunakan user-agent curl dan memasukkan outputnya ke dalam file result.txt
awk -F':"' '/curl/ {print $4}' $log | 
awk '{gsub(/"/,"")}1' | 
awk 'END{printf "Ada %d requests yang menggunakan curl sebagai user-agent\n\n", NR}' >> $path/$res

## E
## Memasukkan daftar IP yang menyerang pada jam 2 pagi  pada tanggal 22 dan memasukkan output kedalam file  result.tx
awk -F':"' 'NR>1{printf "%s %s\n", $1, $2}' $log | sort | 
awk '{gsub(/"/,"")}1' | awk '/22\/Jan\/2022:02:/ {print $1}' | uniq | 
awk '{print}' >> $path/$res

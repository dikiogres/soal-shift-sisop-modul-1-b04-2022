#!/bin/bash
DATETIME=`date '+%Y%m%d%H'`
username=`whoami`
awk '
    BEGIN {
        FS=","
        print "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
    }

    /home/ {
        ++total_file
        home = $(NF-1)
        if (total_column < NF)
            total_column = NF
        for (i=1; i<=NF; i++) {
            if (NR == FNR) {
                max[i] = -99999999
                min[i] = 99999999
                sum[i] = 0
            }

            if (i == (NF-1)) {
                continue
            }
            sum[i] += $i
            if (max[i] < $i) {
                max[i] = $i
            }
            if (min[i] > $i) {
                min[i] = $i
            }
        }
    }

    END {
        for (i=1; i<=3; i++) {
            if (i == 1) {
                printf("minimum,")
            } else if (i == 2) {
                printf("maximum,")
            } else if (i == 3) {
                printf("average,")
            }

            for (j=1; j<=total_column; j++) {
                if (j == (total_column-1)) {
                    printf("%s,", home)
                    continue
                }
                if (i == 1) {
                    printf("%s", min[j])
                } else if (i == 2) {
                    printf("%s", max[j])
                } else if (i == 3) {
                    printf("%s", (sum[j] / total_file))
                }

                if (j != total_column) {
                    printf(",")
                }
            }
            printf("\n")
        }
    }' /home/"$username"/log/metrics_$(date '+%Y%m%d%H')*.log > /home/"$username"/log/"metrics_$DATETIME".log
    
if [ ! -f "aggregate_cron" ]
then
    crontab -l > aggregate_cron
    echo "59 */1 * * * "$(pwd | awk '{ gsub(/ /, "\\ ", $0); print; }')"/$0" >> aggregate_cron
    crontab aggregate_cron
fi

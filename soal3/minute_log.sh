#!/bin/bash

DATETIME=`date '+%Y%m%d%H%M%S'`
username=`whoami`
free -m > temp_memory.log
du -sh /home/$username >> temp_memory.log

file_header="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

echo $file_header > log/metrics_$DATETIME.log

awk 'NR==2 {print $2","$3","$4","$5","$6","$7}' temp_memory.log > temp_line.txt
awk 'NR==3 {print $2","$3","$4}' temp_memory.log >> temp_line.txt
awk 'NR==4 {print $2","$1}' temp_memory.log >> temp_line.txt

paste -d, -s temp_line.txt >> /home/$username/log/metrics_$DATETIME.log

rm temp_memory.log
rm temp_line.txt

if [ ! -f "minute_cron" ]
then
    crontab -l > minute_cron
    echo "* * * * * "$(pwd | awk '{ gsub(/ /, "\\ ", $0); print; }')"/$0" >> minute_cron
    crontab minute_cron
fi

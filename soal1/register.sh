#!/bin/bash

func_username () {

	if ! [ -d "users" ]
	then
	  mkdir users
	fi

	if ! [ -f "users/user.txt" ]
	then
   	  touch users/user.txt
 	fi
 	
	echo "Register Now!"
	echo -n "Enter Username: "
	read username
	
	user=$(awk '$1==temp {++n} END {print n}' temp="$username" users/user.txt)

	if [ -z "$user" ]
	then
  	user=0
  	fi

	if [ $user -gt 0 ]
	then
    	  echo "Username has been used!"
    	  exit
    	  
    	  echo $calendar $time REGISTER: ERROR User already exists >> ./users/log.txt
    	  fi
}

func_password () {
	echo -n "Enter Password: "
  	read -s password

	if [ ${#password} -lt 8 ]
	then
    	  echo -e "\nInvalid password!"
    	exit

	elif [[ $password = ${password,,} || $password = ${password^^} ]]
	then
     	  echo -e "\nInvalid password!"
   	exit

	elif [[ "$password" =~ [^a-zA-Z0-9] ]]
 	then 
    	  echo -e "\nInvalid password!"
    	exit

	elif [[ "$password" == $username ]]
  	then 
    	  echo -e "\nInvalid password!"
    	exit
  
	else 
  	  echo " "
  	  echo "Registered successfully!"
  	  echo "$username $password"  >> ./users/user.txt
  	
  	  echo $calendar $time REGISTER:INFO User $username registered successfully >> ./users/log.txt
  
  	fi 
} 

calendar=$(date +%D)
time=$(date +%T)

func_username
func_password
#!/bin/bash

func_login () {
  echo "==== LOGIN ===="
  echo -n "Username: "
  read username
  echo -n "Password: "
  read -s password

  if ! [ -d "users" ]
  then
    mkdir users
  fi

  if ! [ -f "users/user.txt" ]
  then
    touch users/user.txt
  fi
  
  ceck_password=$(awk '$1==temp {print $2}' temp="$username" users/user.txt)
  if ! [[ "$password" = "$ceck_password" ]]
  then
    echo -e "\nInvalid Password!"
    echo $calendar $time >> ./users/log.txt
    printf "LOGIN: ERROR Failed login attempt on user %s\n" $username >> ./users/log.txt
    exit
  else 
   echo -e "\nHello $username! thanks for login!"
   echo $calendar $time > ./users/log.txt
   printf "LOGIN: INFO User %s logged in\n" $username >> ./users/log.txt
   fi
}

func_command () {
  echo "Choose your Command:"
  echo "- dl N (N = jumlah gambar yang akan didownload)"
  echo "- att (jumlah percobaan login)"
  echo -n "Enter: "
  read command n
  
  if [[ $command = "dl" ]]
  then
    download
    file_zip
  elif [[ $command = "att" ]]
  then 
    awk -v temp=$username '/LOGIN/&&$0~" " temp " " {++n; print} END {print "Login Attempt: " n}' ./users/log.txt
  fi
}

download () {
  while [ $i -le $n ]
  do
    if [ $i -lt 10 ]
    then
      wget https://loremflickr.com/320/240 -O $nama_zip/PIC_$i
    fi
    ((i++))
  done
  zip -r -e $nama_zip $nama_zip
  rm -r $nama_zip
}

file_zip () {
  nama_zip=`printf '%(%Y-%m-%d)T'`_$username
  if [ -f "$nama_zip.zip" ]
  then 
    unzip -e $nama_zip.zip
    i=`ls ./$nama_zip | wc -l`
    n=$((n + i))
  else
    mkdir $nama_zip
    i=1
  fi
}
calendar=$(date +%D)
time=$(date +%T)

func_login
func_command
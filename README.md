# Kelompok B04 Sistem Operasi 2022

### Anggota:
| Nama                               | NRP        |
|------------------------------------|------------|
| Lia Kharisma Putri                 | 5025201034 |
| Muhammad Dzikri Fakhrizal Syairozi | 5025201201 |
| Mohammad Kholid Bughowi            | 5025201253 | 
<br/>

### Soal 1:
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, Pada soal no 1 terdapat studi case dimana han dan teman-temannya di beri tugas untuk mencari foto, untuk mempermudah pengerjaannya maka di buatlah sebuah program dengan menginputkan nama dan password 

#### Poin A 
pada poin a diminta untuk membuat sistem register pada script register.sh dan sistem login yang dibuat di script main.sh
- sistem register
```
func_username () {

	if ! [ -d "users" ]
	then
	  mkdir users
	fi

	if ! [ -f "users/user.txt" ]
	then
   	  touch users/user.txt
 	fi
 	
	echo "Register Now!"
	echo -n "Enter Username: "
	read username
	
	user=$(awk '$1==temp {++n} END {print n}' temp="$username" users/user.txt)

	if [ -z "$user" ]
	then
  	user=0
  	fi

	if [ $user -gt 0 ]
	then
    	  echo "Username has been used!"
    	  exit
    	  
    	  echo $calendar $time REGISTER:INFO User $username registered successfully >> ./users/log.txt
    	  fi
}
```
di atas merupakan sistem registrasi dimana users diminta untuk megiputkan nama, jika registrasi telah berhasil di lakukan maka data user akan di simpan di dalam file ./users/user.txt dan ./users/log.txt
sistem akan membaca file data yang telah tersimpan mengeluarkan output echo "Username has been used!" jika users melakukan registrasi dengan nama yang sama
```
user=$(awk '$1==temp {++n} END {print n}' temp="$username" users/user.txt)
```
menggunakan awk dimana setiap user yang telah melakukan registrasi data disimpan di dalam file ./users/user.txt. 

- sistem login (main.sh)
```
func_login () {
  echo "==== LOGIN ===="
  echo -n "Username: "
  read username
  echo -n "Password: "
  read -s password

  if ! [ -d "users" ]
  then
    mkdir users
  fi

  if ! [ -f "users/user.txt" ]
  then
    touch users/user.txt
  fi
  
  ceck_password=$(awk '$1==temp {print $2}' temp="$username" users/user.txt)
  if ! [[ "$password" = "$ceck_password" ]]
  then
    echo -e "\nInvalid Password!"
    echo $calendar $time >> ./users/log.txt
    printf "LOGIN: ERROR Failed login attempt on user %s\n" $username >> ./users/log.txt
    exit
  else 
   echo -e "\nHello $username! thanks for login!"
   echo $calendar $time > ./users/log.txt
   printf "LOGIN: INFO User %s logged in\n" $username >> ./users/log.txt
   fi
}

….
```
setelah melakukan registrasi maka users dapat langsung login 
#### Poin B 
Diminta untuk menambahkan password pada sistem register dan sistem login agar keamanan lebih terjaga 
- register 
```
func_password () {
	echo -n "Enter Password: "
  	read -s password

	if [ ${#password} -lt 8 ]
	then
    	  echo -e "\nInvalid password!"
    	exit

	elif [[ $password = ${password,,} || $password = ${password^^} ]]
	then
     	  echo -e "\nInvalid password!"
   	exit

	elif [[ "$password" =~ [^a-zA-Z0-9] ]]
 	then 
    	  echo -e "\nInvalid password!"
    	exit

	elif [[ "$password" == $username ]]
  	then 
    	  echo -e "\nInvalid password!"
    	exit
  
	else 
  	  echo " "
  	  echo "Registered successfully!"
  	  echo "$username $password"  >> ./users/user.txt
  	
  	  echo $calendar $time REGISTER:INFO User $username registered successfully >> ./users/log.txt
  
  	fi 
}
```
pada funsi password terdapat beberapa kriteria yaitu :
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username

jika tidak memenuhi kriteria maka sistem akan mengeluarkan ouput berupa “Invalid Password!” jika password telah memenuhi kriteria maka registrasi berhasil dan sistem akan mengeluarkan ouput “Registered successfully!”
- login/ main.sh
```
ceck_password=$(awk '$1==temp {print $2}' temp="$username" users/user.txt)
  if ! [[ "$password" = "$ceck_password" ]]
```
sistem login akan membaca file users/user.txt untuk mengecek apakah password sama dengan pada saat registrasi 

#### Poin C
Pada poin cinta agar setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
```
echo $calendar $time REGISTER: ERROR User  already exists >> ./users/log.txt
```
ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
```
echo $calendar $time REGISTER:INFO User $username registered successfully >> ./users/log.txt
```
Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
```
printf "LOGIN: ERROR Failed login attempt on user %s\n" $username >> ./users/log.txt
```
ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
```
printf "LOGIN: INFO User %s logged in\n" $username >> ./users/log.txt
```
Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

#### Poin D
pada poin d di berikan link untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user.  dan juga menggitung jumlah percobaan login

```
download () {
  while [ $i -le $n ]
  do
    if [ $i -lt 10 ]
    then
      wget https://loremflickr.com/320/240 -O $nama_zip/PIC_$i
    fi
    ((i++))
  done
  zip -r -e $nama_zip $nama_zip
  rm -r $nama_zip
}

file_zip () {
  nama_zip=`printf '%(%Y-%m-%d)T'`_$username
  if [ -f "$nama_zip.zip" ]
  then 
    unzip -e $nama_zip.zip
    i=`ls ./$nama_zip | wc -l`
    n=$((n + i))
  else
    mkdir $nama_zip
    i=1
  fi
}

```
Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ). Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.

```
awk -v temp=$username '/LOGIN/&&$0~" " temp " " {++n; print} END {print "Login Attempt: " n}' ./users/log.txt
```
sistem akan membaca dan menghitung percobaan login baik yang berhasil maupun yang gagal pada file ./user/log.txt untuk mengetahui att (jumlah percobaan login)


### Soal 2:
Diberikan sebuah log file yang berisi sekumpulan request ke website https://daffa.info yang dilakukan pada tanggal 22 Januari 2022.

Pada soal nomer 2 terdapat 5 poin permasalahan. Beberapa permasalahan dapat diselesaikan dengan beberapa logika umum sebagai berikut:
\- Declare variable
path=forensic_log_website_daffainfo_log
log=~/log_website_daffainfo.log
avg=ratarata.txt
res=result.txt
\- Format log dipisah dengan awk berdasarkan titik dua (:)
\- Format log dipisah dengan awk berdasarkan titik dua diikuti petik dua (:")

Dengan beberapa logika tersebut, maka poin poin persoalan pada nomor 2 dapat diselesaikan. Kedua cara tersebut nantinya akan digunakan sebagai referensi dalam penjelasan nomor 2 ini.

#### Poin a
Pada poin a diminta untuk membuat directory bernama forensic_log_website_daffainfo_log
```
[ ! -d $path ] && mkdir $path
[ ! -f $path/$avg ] && touch $path/$avg
[ ! -f $path/$res ] && touch $path/$res
```
#### Poin b
Pada poin b diminta untuk menghitung rata - rata serangan tiap jam. 
```
awk -F: 'NR>1{print $3}'  $log| sort | uniq -c | 
awk '{sum+=$1} END {printf "Rata-rata serangan adalah sebanyak %d requests per jam\n", sum/(NR-1)}' >> $path/$avg
```
Logika pada poin b adalah:
- `awk -F: 'NR>1{print $3}'  $log` Memecah log berdasarkan titik dua (:) mulai dari baris kedua pada log file kemudian mengambil variabel $3 
- Hasil diurutkan secara leksikografis dengan syntax `sort`. 
- Dihitung banyaknya kemunculan tiap jam yang berbeda dengan fungsi `uniq -c`, misal banyak data dengan jam 00 adalah sebanyak 12. 

- Dicari rata rata dengan menjumlahkan semua nilai pada kolom 1 kemudian dibagi dengan jumlah baris. Operasi tersebut dapat dilakukan dengan  `awk`. Hasil operasi awk kemudian dimasukkan ke dalam file rata-rata.txt.

#### Poin c:
Pada poin c diminta untuk mencari IP yang paling banyak melakukan request ke server dan menampilkan banyaknya request yang dikirim dari IP tersebut.
```
awk -F: 'NR>1{print $1}' $log | sort | uniq -c | sort -rn | 
awk 'NR==1{gsub(/"/,"")}1' | awk 'NR==1{printf "IP yang paling banyak mengakses server adalah:  %s sebanyak %s requests\n\n", $2, $1}' >> $path/$res
```
Logika pada poin c adalah:
- `awk -F: 'NR>1{print $1}' $log` Memecah log berdasarkan titik dua (:) mulai dari baris kedua pada log file kemudian mengambil variabel $1 
- `| sort` Melakukan pengurutan secara leksikografis terhadap hasil awk sebelumnya
- `| uniq -c` Menghitung banyak kemunculan tiap IP yang berbeda
- `| sort -rn` Melakukan pengurutan secara menurun berdasarkan banyaknya kemunculan tiap IP
- `awk 'NR==1{gsub(/"/,"")}1'` Menghapus semua petik dua pada baris pertama
- `| awk 'NR==1{printf "IP yang paling banyak mengakses server adalah:  %s sebanyak %s requests\n\n", $2, $1}' > $path/$res` Mengambil baris pertama (IP paling sering muncul) dengan $1 adalah banyak kemunculan dan $2 adalah IP tersebut. Selanjutnya hasil dimasukkan ke dalam file result.txt

#### Poin d
Pada poin ini diminta untuk menghitung banyaknya request yang menggunakan curl sebagai user-agent
```
awk -F':"' '/curl/ {print $4}' $log | 
awk '{gsub(/"/,"")}1' | 
awk 'END{printf "Ada %d requests yang menggunakan curl sebagai user-agent\n\n", NR}' >> $path/$res
```
Logika pada poin d adalah:
- `awk -F':"' '/curl/ {print $4}' $log` Memecah log berdasarkan titik dua diikuti petik dua (:") ambil semua $4 yang mengandung curl
- `| awk 'END{printf "Ada %d requests yang menggunakan curl sebagai user-agent\n\n", NR}' >> $path/$res` Hasil dari operasi sebelumnya dihitung barisnya dengan variabel bawaan NR, kemudian ditampilkan sesuai format pada soal. Setelah itu ditambahkan pada result.txt

#### Poin e
Pada poin ini diminta untuk menampilkan semua request yang dilakukan pada jam 2 pagi.
```
awk -F':"' 'NR>1{printf "%s %s\n", $1, $2}' $log | sort | 
awk '{gsub(/"/,"")}1' | awk '/22\/Jan\/2022:02:/ {print $1}' | uniq | 
awk '{print}' >> $path/$res
```
Logika pada poin e adalah:
- `awk -F':"' 'NR>1{printf "%s %s\n", $1, $2}' $log` Memisah log berdasarkan titik dua diikuti petik dua (:") kemudian mengambil $1 dan $2
- `| sort` Mengurutkan hasil dari operasi sebelumnya secara leksikografis
- `| awk '{gsub(/"/,"")}1'` Menghapus semua titik dua
- `| awk '/22\/Jan\/2022:02:/ {print $1}'` Mengambil semua baris yang mengandung `22/Jan/2022:02:` kemudian mengambil $1 yaitu IP
- `| uniq` Memisahkan IP yang unik, sehingga tidak akan ada IP yang sama muncul dua kali
- `awk '{print}' >> $path/$res` Memasukkan hasil kedalam result.txt


### Soal 3:
Pada soal no.3, diminta untuk membuat `minute_log.sh` yang dijalankan tiap menit untuk merecord penggunaan memory dengan `free -m` dan resource penyimapanan di `home/{user}` dengan `du -sh <path>`. Log tiap menit disimpan ke folder log dengan nama file `metrics_Y%m%d%H%M%S`. Berikut syntax untuk membuat file log tersebut dan menuliskan header seperti mem_total,mem_used,mem_free, dst.
```shell
DATETIME=`date '+%Y%m%d%H%M%S'`
file_header="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
echo $file_header > log/metrics_$DATETIME.log
```
Untuk output dari `free -m` dan `du -sh <path>` disimpan kedalam file `temp_memory.log`. Kemudian dengan awk dilakukan perubahan format penulisan menjadi seperti `mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size` dst. Namun, karena kesulitan untuk menjadikannya kedalam format tersebut secara langsung, maka dibuat file `temp_line.txt` untuk menyimpan part-part nya. Baru nanti diakhir digabung semua menjadi satu.
```shell
free -m > temp_memory.log
du -sh /home/$username >> temp_memory.log

awk 'NR==2 {print $2","$3","$4","$5","$6","$7}' temp_memory.log > temp_line.txt
awk 'NR==3 {print $2","$3","$4}' temp_memory.log >> temp_line.txt
awk 'NR==4 {print $2","$1}' temp_memory.log >> temp_line.txt

paste -d, -s temp_line.txt >> /home/$username/log/metrics_$DATETIME.log
```
Kemudian agar `minute_log.sh` ini dijalankan tiap menit, berikut syntax untuk membuat cron jobs nya
```shell
crontab -l > minute_cron
echo "* * * * * "$(pwd | awk '{ gsub(/ /, "\\ ", $0); print; }')"/$0" >> minute_cron
crontab minute_cron
```

Selanjutnya, dibuat `aggregate_minutes_to_hourly_log.sh` untuk menghitung minimum, maximum, dan average dari tiap kolom `mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size`. Didapatkan semua file metrics log dengan awk yang nama file nya terdapat elemen `Y%m%d%H` dengan syntax berikut.
```shell
metrics_$(date '+%Y%m%d%H')*.log
```

Dengan awk juga, didapatkan `total_file` yang dibaca, menampung nilai minimum tiap kolom kedalam array `min`, menampung nilai maximum tiap kolom kedalam array `max`, dan menjumlah semua nilai pada tiap kolom yang sama dan disimpan pada array `sum`.
```shell
++total_file
home = $(NF-1)
if (total_column < NF)
    total_column = NF
for (i=1; i<=NF; i++) {
    if (NR == FNR) {
        max[i] = -99999999
        min[i] = 99999999
        sum[i] = 0
    }
    if (i == (NF-1)) {
        continue
    }
    sum[i] += $i
    if (max[i] < $i) {
        max[i] = $i
    }
    if (min[i] > $i) {
        min[i] = $i
    }
}
```

Setelah semua proses perhitungan selesai, kita print hasilnya ke file log dengan nama `metrics_%Y%m%d%H`.
```shell
for (i=1; i<=3; i++) {
    if (i == 1) {
        printf("minimum,")
    } else if (i == 2) {
        printf("maximum,")
    } else if (i == 3) {
        printf("average,")
    }
    for (j=1; j<=total_column; j++) {
        if (j == (total_column-1)) {
            printf("%s,", home)
            continue
        }
        if (i == 1) {
            printf("%s", min[j])
        } else if (i == 2) {
            printf("%s", max[j])
        } else if (i == 3) {
            printf("%s", (sum[j] / total_file))
        }

        if (j != total_column) {
            printf(",")
        }
    }
    printf("\n")
}
```

Terakhir, karena kita juga perlu menjalankan file ini tiap jam, maka dibuatlah cron job nya seperti berikut
```shell
crontab -l > aggregate_cron
echo "59 */1 * * * "$(pwd | awk '{ gsub(/ /, "\\ ", $0); print; }')"/$0" >> aggregate_cron
crontab aggregate_cron
```

#### Kendala
Kendalanya ada pada numeric format yang digunakan pada linux. Apabila user menggunakan region Indonesia, maka numeric format untuk bilangan desimalnya adalah 12,5 (menggunakan ","). Sedangkan script ini hanya mendukung numeric format dengan ".". Contoh 123.5.
